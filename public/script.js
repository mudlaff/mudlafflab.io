// wyświetli nam pop up z tekstem
alert("Hello World");

// wyswietli komunikat w konsoli w dev ops
console.log("Hellof from console");
console.log(3 + 574890);
console.log(546987 * 870932);

console.log(20 % 5);

// tworzenie zmiennej. Zeby ja stworzyc uzywamy slowa let na poczatku
let wynik = 10 * 10;
console.log("Wynik wynosi " + wynik);

// wartość zmiennej można zmieniac juz bez słowa let na początku
wynik = 1;
console.log("Wynik po zmianie to " + wynik);

// tworzenie stałej, której nie można już zmienić. na poczatku musi byc const. Przy uzywaniu jej na pozniejszym etapie nie potrzeba juz uzywac const
const stala = 233;

// wyskakujace okienko z pytaniem i miejscem na odpowiedz
let userName = prompt("Jak masz na imie?");
console.log("Twoje imię to " + userName);

// tworzenie tablic w których mozna przechowywac wiecej danych
let tablica = [1, 2, 3, 5, 12, 85, 164169, "jejku jejku", 152.4];

// wywolanie elementu tablicy, ktory jet na indeksie 5 (indeksy liczymy od 0)
console.log(tablica[5]);

// przejscie po wszystkich elementach tablicy z pomoca petli. Wpisująć IN podciąga nam numer indexu
for (let i in tablica) {
  console.log(tablica[i] + " jest " + i + " elementem w tablicy");
}

// przejscie po wszystkich elementach tablicy z pomoca petli. Wpisująć OF podciąga się konkretny element, jego wartość
for (let j of tablica) {
  console.log([j] + " jest " + j + " elementem w tablicy");
}

// kolejna tablica
let listaZadan = ["kupic mleko", "wyprowadzic psa", "odkurzyc"];

// znajduje element z ID main
let mainElement = document.getElementById("main");
console.log(mainElement);

// znajduje elementy z tagiem <p>
let pElements = document.getElementsByTagName("p");
console.log(pElements);

// znajduje elementy po klasie
let classElements = document.getElementsByClassName("middle-one");
console.log(classElements);

// wewnatrz klasy znajduje konkretne tagi w tym przypadku <div>
console.log(classElements[0].getElementsByTagName("div"));

/* zmiana zawartosci (w tym przypadku tekstu) w wyszukanym elemencie (pierwszy powyzej)
 */
mainElement.innerText = "Witaj " + userName;

// Tworzenie funkcji i jej wywołanie:
function log(logMessage1, logMessage2) {
  console.log(logMessage1 + new Date() + logMessage2);
}

log("Begining ", " End");

// tworzenie funkcji, ktora przyjmuje inna funkcje jako parametr plus jej wywołanie:
function outerLog(callback, logMessage1, logMessage2) {
  console.log("No to zaczynamy ");
  callback(logMessage1, logMessage2);
  console.log("Dziękuję za uwagę");
}

outerLog(log, "TEST ", " END OF TEST");

/*
// funkcja setTimeout. Bedziemy potrebować innej funkcji, która zostaniejej parametrem. 
// Dodatkowa funkcja bedzie wywolywac alert
function timeout() {
    alert("Uwaga! Czas mija");
}

// 5000 oznacza 5000 ms czyli 5 s
setTimeout(timeout, 3000);
*/

/* 
//Wewnątrz setTimeout można zagnieździć funkcję anonimową. To znaczy że w ramach parametru tworzymy 
//nową funkcję.
//UWAGA! czas liczony jest od początku a nie od momentu zatwierdzenia pierwszego komunikatu 
setTimeout(function () {alert("Funkja anonimowa");}, 5000);
*/

/* 
//można tez stworzyć funkcję anonimową wewnatrz zmiennej. wtedy wyglądałoby to tak:
let timeout = function () {alert("Funkja anonimowa")};
timeout()
setTimeout(timeout, 5000);
*/

/*
// Jeżeli instrukcja jest prosta mozna skorzystać z funkcji strzałkowej
setTimeout(() => alert("UWAGA! czas mija"), 5000);
*/

// funkcję strzałkową można przypisać do zmiennej:
let timeout = () => alert("UWAGA! czas mija");
setTimeout(timeout, 5000);

let id = null;

// napisane za pomocą javascripu. Wywoływanie elementów następuje poprzez komendy document. getElementById
// jest to bardziej praco i czasochłonne

// let reload = () => {
//   if (id > 0) {
//     clearTimeout(id);
//   }
//   clearTimeout(id);
//   fetch("https://api.chucknorris.io/jokes/random")
//     .then((r) => r.json())
//     // .then((r) => console.log(r.value))
//     .then((r) => {
//       document.getElementById("joke").innerText = r.value;
//     });
//   id = setTimeout(reload, 5000);
// };

// reload();

// document.getElementById("button").onclick = reload;

// to samo co powyżej ale napisane z pomocą jquery. działa tak samo
// zamiast komend uzywamy znaku dolara i elementy wywołujemy # albo .

let reload = () => {
  if (id > 0) {
    clearTimeout(id);
  }
  $("#joke").slideUp(() => {
    clearTimeout(id);
    fetch("https://api.chucknorris.io/jokes/random")
      .then((r) => r.json())
      // .then((r) => console.log(r.value))
      .then((r) => {
        $("#joke").html(r.value);
        $("#joke").slideDown();
      });

    id = setTimeout(reload, 5000);
  });
};

reload();

// czasem gdy mamy wiecej bibliotek to one mogą też używać znaku $ i zaczynają się kłócić
// w takim przypadku taki zapis:
$("#button").click(reload);
// wyglądałby tak:
// jQuery("#button").click(reload);
// wpisujemy jQuery zamiast  i działa tak samo
